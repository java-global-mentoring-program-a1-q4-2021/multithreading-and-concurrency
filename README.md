# Multithreading and Concurrency

## Self-study materials

Basics of multi-threading
https://videoportal.epam.com/video/6RnN3GRr

Advanced dive into multi-threading and concurrency
https://videoportal.epam.com/video/nRmWY1a9

Concurrency progression over the years
https://videoportal.epam.com/video/bYqdGBae


## Books:
Java-Concurrency-Practice-Brian-Goetz
https://www.amazon.com/Java-Concurrency-Practice-Brian-Goetz/dp/0321349601

Art-Multiprocessor-Programming
https://www.amazon.com/Art-Multiprocessor-Programming-Revised-Reprint/dp/0123973376


## Documentations:
The Java™ Tutorials
https://docs.oracle.com/javase/tutorial/essential/concurrency/


## Sites:
JMM
https://shipilev.net/blog/2014/jmm-pragmatics/

Set of articles that cover most of the required topics
https://www.baeldung.com/java-concurrency
