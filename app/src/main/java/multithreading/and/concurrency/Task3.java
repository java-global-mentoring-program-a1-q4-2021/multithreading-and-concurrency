package multithreading.and.concurrency;

import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

public class Task3 {

    public static void main(String[] args) {
        MessageBus messageBus = new MessageBus();

        Thread producerA = new Thread(new Producer("A", messageBus));
        producerA.start();

        Thread producerB = new Thread(new Producer("B", messageBus));
        producerB.start();

        Thread consumerA = new Thread(new Consumer("A", messageBus));
        consumerA.start();

        Thread consumerB = new Thread(new Consumer("B", messageBus));
        consumerB.start();
    }

    private static void _wait(Object o) {
        try {
            o.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void _sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static class MessageBus {
        private final boolean flag = true;
        private final Map<String, Queue<String>> queueMap = new HashMap<>();

        public synchronized void produce(String topic, String message) {
            if (!flag) {
                _wait(this);
            }

            if (queueMap.containsKey(topic)) {
                queueMap.get(topic).add(message);
            } else {
                Queue<String> queue = new ArrayDeque<>();
                queue.add(message);
                queueMap.put(topic, queue);
            }

            this.notifyAll();
        }

        public synchronized String consume(String topic) {
            if (flag) {
                _wait(this);
            }

            if (!queueMap.containsKey(topic)) {
                return null;
            }

            this.notifyAll();
            return queueMap.get(topic).poll();
        }
    }

    private static class Producer implements Runnable {
        private final String topic;
        private final MessageBus messageBus;

        public Producer(String topic, MessageBus messageBus) {
            this.topic = topic;
            this.messageBus = messageBus;
        }

        @Override
        public void run() {
            while (true) {
                String msg = LocalDateTime.now() + "   Hello world!";
                messageBus.produce(topic, msg);

//                _sleep(5);
            }
        }
    }

    private static class Consumer implements Runnable {
        private final String topic;
        private final MessageBus messageBus;

        public Consumer(String topic, MessageBus messageBus) {
            this.topic = topic;
            this.messageBus = messageBus;
        }

        @Override
        public void run() {
            while (true) {
                while (true) {
                    System.out.println("Message was received from " + topic + ": " + messageBus.consume(topic));

//                    _sleep(5);
                }
            }
        }
    }
}
