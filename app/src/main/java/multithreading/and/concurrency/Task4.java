package multithreading.and.concurrency;


import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class Task4 {

    public static void main(String[] args) {
        SomeObjectPool pool = new SomeObjectPool(2);
        new Thread(pool::get).start();
        new Thread(() -> {
            _sleep(2);
            pool.get();
        }).start();
        SomeObject o = pool.get();
        _sleep(5);
        pool.take(o);
    }

    private static void _wait(Object o) {
        try {
            o.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void _sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static class SomeObject {
        private static final AtomicInteger counter = new AtomicInteger(0);

        public SomeObject() {
            String s = "This is object number " + counter.incrementAndGet();
            System.out.println(s);
        }
    }

    public static class SomeObjectPool extends BlockingObjectPool<SomeObject> {
        public SomeObjectPool(int size) {
            super(size);
        }

        @Override
        protected SomeObject create() {
            return new SomeObject();
        }
    }

    /**
     * Pool that block when it has not any items or it full
     */
    public static abstract class BlockingObjectPool<T> {
        private final Set<T> available = new HashSet<>();
        private final Set<T> inUse = new HashSet<>();

        /**
         * Creates filled pool of passed size * * @param size of pool
         */
        public BlockingObjectPool(int size) {
            for (int i = 0; i < size; i++) {
                available.add(create());
            }
        }

        protected abstract T create();

        /**
         * Gets object from pool or blocks if pool is empty * * @return object from pool
         */
        public synchronized T get() {
            if (available.isEmpty()) {
                _wait(this);
            }

            T t = available.iterator().next();
            available.remove(t);
            inUse.add(t);

            this.notifyAll();
            return t;
        }

        /**
         * Puts object to pool or blocks if pool is full * * @param object to be taken back to pool
         */
        public synchronized void take(T t) {
            if (inUse.isEmpty()) {
                _wait(this);
            }

            inUse.remove(t);
            available.add(t);
            this.notifyAll();
        }
    }

}
