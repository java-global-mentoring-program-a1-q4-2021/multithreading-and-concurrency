package multithreading.and.concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Task2 {
    private static final List<Integer> integers = Collections.synchronizedList(new ArrayList<>());
    private static char flag = '1';

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            Random random = new Random();
            while (true) {
                synchronized (integers) {
                    if (flag == '1') {
                        int number = random.nextInt(10);
                        integers.add(number);
                        flag = '2';
                        System.out.println("Random number: " + number);
                        _sleep(2);
                        integers.notifyAll();
                    } else {
                        _wait(integers);
                    }
                }
            }
        }, "Thread-1");
        thread1.start();

        Thread thread2 = new Thread(() -> {
            while (true) {
                synchronized (integers) {
                    if (flag == '2') {
                        System.out.println("Sum of numbers: "
                                + integers.stream().mapToLong(Integer::longValue).sum());
                        flag = '3';
                        _sleep(2);
                        integers.notifyAll();
                    } else {
                        _wait(integers);
                    }
                }
            }
        }, "Thread-2");
        thread2.start();

        Thread thread3 = new Thread(() -> {
            while (true) {
                synchronized (integers) {
                    if (flag == '3') {
                        System.out.println("Square root of sum of squares of all numbers: "
                                + Math.sqrt(integers.stream().mapToLong(v -> (long) v * v).sum()));
                        flag = '1';
                        _sleep(2);
                        integers.notifyAll();
                    } else {
                        _wait(integers);
                    }
                }
            }
        }, "Thread-3");
        thread3.start();
    }

    private static void _wait(Object o) {
        try {
            o.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void _sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
